*** Settings ***
Documentation   This suite file verfies that the valid users are able to login dashboard
...     and connected to the test case TC_0H_02

Resource        ../../resource/base/CommonFunctionalities.resource

Test Setup      Launch browser and nevigate to url
Test Teardown       Close Browser

Test Template   Verify valid login Template

*** Test Cases ***
TC1         Admin       admin123      Dashboard
TC2         rohit1       rawat1      Invalid credentials
TC3         Admin       admin123      Dashboard

*** Keywords ***
Verify valid login Template
    [Arguments]     ${username}     ${password}     ${expected_output}
    Input Text    //input[@name="username"]    ${username}
    Input Text    //input[@name="password"]    ${password}
    Click Element    //button[@type="submit"]
    Element Text Should Be    //h6[contains(normalize-space(),'Dashboard')]     ${expected_output}