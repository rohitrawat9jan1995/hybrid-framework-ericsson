*** Settings ***
Documentation   This suite file verfies that the valid users are able to login dashboard
...     and connected to the test case TC_0H_02

Resource        ../../resource/base/CommonFunctionalities.resource
Library     DataDriver      file=../../test_data/invalid_login_csv.csv       sheet_name=Sheet1

Test Setup      Launch browser and nevigate to url
Test Teardown       Close Browser

Test Template   Verify Valid login template

*** Test Cases ***
TC ${tc_number}

*** Keywords ***
Verify Valid login template
    [Arguments]     ${username}     ${password}     ${expected_output}
    Input Text    //input[@name="username"]    ${username}
    Input Text    //input[@name="password"]    ${password}
    Click Element    //button[@type="submit"]
    #For validation
    Element Text Should Be    //p[contains(normalize-space(),'Invalid credentials')]     ${expected_output}