*** Settings ***
Documentation   This suite file verfies that the valid users are able to login dashboard
...     and connected to the test case TC_0H_02

Resource        ../../resource/base/CommonFunctionalities.resource

Test Setup      Launch browser and nevigate to url
Test Teardown       Close Browser

*** Test Cases ***
Verify valid login test
    Input Text    //input[@name="username"]    Admin
    Input Text    //input[@name="password"]    admin1234
    Click Element    //button[@type="submit"]
    Element Text Should Be    //p[contains(normalize-space(),'Invalid credentials')]     Invalid credentials