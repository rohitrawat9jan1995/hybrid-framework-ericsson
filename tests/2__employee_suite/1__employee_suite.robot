*** Settings ***
Documentation   Employee details
Resource        ../../resource/base/CommonFunctionalities.resource

*** Test Cases ***
TC1
    Launch browser and nevigate to url
    Input Text    //input[@name="username"]    Admin
    Input Text    //input[@name="password"]    admin123
    Click Element    //button[@type="submit"]
    Element Text Should Be    //h6[contains(normalize-space(),'Dashboard')]     Dashboard
    Click Element    //span[contains(normalize-space(),'PIM')]
    Click Element    //a[contains(normalize-space(),'Add Employee')]
    Input Text    //input[@name="firstName"]    john
    Input Text    //input[@name="middleName"]    a
    Input Text    //input[@name="lastName"]    wick
    Choose File    xpath=//input[@type="file"]     C:${/}Automation Concepts${/}john.jpg
    Click Element    //button[contains(normalize-space(),'Save')]
    #without sleep it was not working
    Sleep    5s
    Element Text Should Be    //div[@class="orangehrm-edit-employee-name"]      john wick

*** Comments ***
 1. Add Employee header should be displayed
 2. Employee First Name should be displayed in the text box